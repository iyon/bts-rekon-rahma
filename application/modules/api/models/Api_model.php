<?php
class Api_model extends MY_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_rekon_total() {
      $sql = $this->db->query("SELECT g.REGIONAL as REGIONAL, r.REKON as REKON, t.TOTAL as TOTAL
          FROM ( SELECT REGIONAL FROM t_sum_bts_nodeb_final WHERE REGIONAL != '' GROUP BY REGIONAL)	as g
          LEFT JOIN ( SELECT REGIONAL, COUNT(*) AS REKON FROM t_sum_bts_nodeb_final WHERE status in ('5','6') GROUP by REGIONAL ) as r ON r.REGIONAL=g.REGIONAL
      	  LEFT JOIN ( SELECT REGIONAL, COUNT(*) AS TOTAL FROM t_sum_bts_nodeb_final GROUP by REGIONAL )	as t ON t.REGIONAL=g.REGIONAL ORDER BY SUBSTRING(g.REGIONAL,9,2)*10 ASC");
      return $sql->result();
    }

    public function get_openclose_total() {
      $sql = $this->db->query("SELECT g.REGIONAL as REGIONAL, r.OPEN as OPEN, t.CLOSE as CLOSE
          FROM ( SELECT REGIONAL FROM t_sum_bts_nodeb_final  WHERE REGIONAL != '' GROUP BY REGIONAL )	as g
          LEFT JOIN ( SELECT REGIONAL, COUNT(*) AS `OPEN` FROM t_sum_bts_nodeb_final WHERE APPROVAL_STATUS = 0 GROUP by REGIONAL ) as r ON r.REGIONAL=g.REGIONAL
        	LEFT JOIN ( SELECT REGIONAL, COUNT(*) AS `CLOSE` FROM t_sum_bts_nodeb_final WHERE APPROVAL_STATUS = 1 GROUP by REGIONAL )	as t ON t.REGIONAL=g.REGIONAL ORDER BY SUBSTRING(g.REGIONAL,9,2)*10 ASC");
      return $sql->result();
    }

    public function get_onair() {
      $sql = $this->db->query("SELECT g.REGIONAL as REGIONAL, r.ONAIR as ONAIR, t.TOTAL as TOTAL
        FROM ( SELECT REGIONAL FROM t_sum_bts_nodeb_final WHERE REGIONAL != '' GROUP BY REGIONAL ) as g
        LEFT JOIN ( SELECT REGIONAL, COUNT(*) AS ONAIR FROM t_sum_bts_nodeb_final WHERE status in ('1','2','3') GROUP by REGIONAL ) as r ON r.REGIONAL=g.REGIONAL
	      LEFT JOIN ( SELECT REGIONAL, COUNT(*) AS TOTAL FROM t_sum_bts_nodeb_final GROUP by REGIONAL )	as t ON t.REGIONAL=g.REGIONAL ORDER BY SUBSTRING(g.REGIONAL,9,2)*10 ASC");
      return $sql->result();
    }

    public function get_dismantle() {
      $sql = $this->db->query("SELECT g.REGIONAL as REGIONAL, r.DISMANTLE as DISMANTLE, t.TOTAL as TOTAL
        FROM ( SELECT REGIONAL FROM t_sum_bts_nodeb_final WHERE REGIONAL != '' GROUP BY REGIONAL ) as g
        LEFT JOIN ( SELECT REGIONAL, COUNT(*) AS DISMANTLE FROM t_sum_bts_nodeb_final WHERE status in ('9') GROUP by REGIONAL ) as r ON r.REGIONAL=g.REGIONAL
	      LEFT JOIN ( SELECT REGIONAL, COUNT(*) AS TOTAL FROM t_sum_bts_nodeb_final GROUP by REGIONAL )	as t ON t.REGIONAL=g.REGIONAL ORDER BY SUBSTRING(g.REGIONAL,9,2)*10 ASC");
      return $sql->result();
    }

    public function get_onair_new() {
      $sql = $this->db->query("SELECT g.REGIONAL as REGIONAL, r.NEW as NEW, t.TOTAL as TOTAL
        FROM ( SELECT REGIONAL FROM t_sum_bts_nodeb_final WHERE REGIONAL != '' GROUP BY REGIONAL ) as g
        LEFT JOIN ( SELECT REGIONAL, COUNT(*) AS NEW FROM t_sum_bts_nodeb_final WHERE status in ('1') GROUP by REGIONAL ) as r ON r.REGIONAL=g.REGIONAL
	      LEFT JOIN ( SELECT REGIONAL, COUNT(*) AS TOTAL FROM t_sum_bts_nodeb_final GROUP by REGIONAL )	as t ON t.REGIONAL=g.REGIONAL ORDER BY SUBSTRING(g.REGIONAL,9,2)*10 ASC");
      return $sql->result();
    }

    public function get_onair_existing() {
      $sql = $this->db->query("SELECT g.REGIONAL as REGIONAL, r.EXISTING as EXISTING, t.TOTAL as TOTAL
        FROM ( SELECT REGIONAL FROM t_sum_bts_nodeb_final WHERE REGIONAL != '' GROUP BY REGIONAL ) as g
        LEFT JOIN ( SELECT REGIONAL, COUNT(*) AS EXISTING FROM t_sum_bts_nodeb_final WHERE status in ('2') GROUP by REGIONAL ) as r ON r.REGIONAL=g.REGIONAL
	      LEFT JOIN ( SELECT REGIONAL, COUNT(*) AS TOTAL FROM t_sum_bts_nodeb_final GROUP by REGIONAL )	as t ON t.REGIONAL=g.REGIONAL ORDER BY SUBSTRING(g.REGIONAL,9,2)*10 ASC");
      return $sql->result();
    }

    public function get_summary_all() {
        $existing_2g = $this->db->query('select sum(NE_QTY) as total from t_sum_bts_nodeb_final where FREQ = "2G" and STATUS = 2')->result();
        // $new_2g = $this->db->query('select sum(NE_QTY) as total from t_sum_bts_nodeb_final where FREQ = "2G" and STATUS = 1')->result();
        $new_2g = 0;
        $dismantle_2g = $this->db->query('select sum(NE_QTY) as total from t_sum_bts_nodeb_final where FREQ = "2G" and STATUS = 9')->result();
        $total_on_2g = $existing_2g[0]->total + $new_2g;

        $existing_3g = $this->db->query('select sum(NE_QTY) as total from t_sum_bts_nodeb_final where FREQ = "3G" and STATUS = 2')->result();
        // $new_3g = $this->db->query('select sum(NE_QTY) as total from t_sum_bts_nodeb_final where FREQ = "3G" and STATUS = 1')->result();
        $new_3g = 0;
        $dismantle_3g = $this->db->query('select sum(NE_QTY) as total from t_sum_bts_nodeb_final where FREQ = "3G" and STATUS = 9')->result();
        $total_on_3g = $existing_3g[0]->total + $new_3g;

        $existing_4g = $this->db->query('select sum(NE_QTY) as total from t_sum_bts_nodeb_final where FREQ = "4G" and STATUS = 2')->result();
        $new_4g = $this->db->query('select sum(NE_QTY) as total from t_sum_bts_nodeb_final where FREQ = "4G" and STATUS = 1')->result();
        $dismantle_4g = $this->db->query('select sum(NE_QTY) as total from t_sum_bts_nodeb_final where FREQ = "4G" and STATUS = 9')->result();
        $total_on_4g = $existing_4g[0]->total + $new_4g[0]->total;

        $existing_total = $existing_2g[0]->total + $existing_3g[0]->total + $existing_4g[0]->total;
        $new_total = $new_2g + $new_3g + $new_4g[0]->total;
        $dismantle_total = $dismantle_2g[0]->total + $dismantle_3g[0]->total + $dismantle_4g[0]->total;
        $total_on_total = $existing_total + $new_total;

        $data = [
            'existing_2g' => intval($existing_2g[0]->total),
            // 'new_2g' => intval($new_2g[0]->total),
            'new_2g' => intval($new_2g),
            'dismantle_2g' => intval($dismantle_2g[0]->total),
            'total_on_2g' => intval($total_on_2g),
            'existing_3g' => intval($existing_3g[0]->total),
            // 'new_3g' => intval($new_3g[0]->total),
            'new_3g' => intval($new_3g),
            'dismantle_3g' => intval($dismantle_3g[0]->total),
            'total_on_3g' => intval($total_on_3g),
            'existing_4g' => intval($existing_4g[0]->total),
            'new_4g' => intval($new_4g[0]->total),
            'dismantle_4g' => intval($dismantle_4g[0]->total),
            'total_on_4g' => intval($total_on_4g),
            'existing_total' => intval($existing_total),
            'new_total' => intval($new_total),
            'dismantle_total' => intval($dismantle_total),
            'total_on_total' => intval($total_on_total)
        ];

        return $data;
    }

    public function get_davinci_traffic() {
      $sql = $this->db->query("");
      return $sql->result();
    }

}
