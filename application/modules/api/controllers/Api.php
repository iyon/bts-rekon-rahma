<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('Api_model');
	}


	public function get_rekon_total() {

		$result = $this->Api_model->get_rekon_total();
		$rdata = array();
		if ($result){
			foreach ($result as $row) {
				$rdata[] =  array(
					'REGIONAL' => $row->REGIONAL,
					'REKON' => (int)$row->REKON,
					'NONREKON' => (int)$row->TOTAL-$row->REKON,
					'TOTAL' => (int)$row->TOTAL
				);
			}
			print json_encode($rdata);
		}else{
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}


	public function get_openclose_total() {

		$result = $this->Api_model->get_openclose_total();
		$rdata = array();
		if ($result){
			foreach ($result as $row) {
				$rdata[] =  array(
					'REGIONAL' => $row->REGIONAL,
					'OPEN' => (int)$row->OPEN,
					'CLOSE' => (int)$row->CLOSE
				);
			}
			print json_encode($rdata);
		}else{
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}

	public function get_onair() {

		$result = $this->Api_model->get_onair();
		$rdata = array();
		if ($result){
			foreach ($result as $row) {
				$rdata[] =  array(
					'REGIONAL' => $row->REGIONAL,
					'ONAIR' => (int)$row->ONAIR,
					'TOTAL' => (int)$row->TOTAL
				);
			}
			print json_encode($rdata);
		}else{
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}

	public function get_dismantle() {

		$result = $this->Api_model->get_dismantle();
		$rdata = array();
		if ($result){
			foreach ($result as $row) {
				$rdata[] =  array(
					'REGIONAL' => $row->REGIONAL,
					'DISMANTLE' => (int)$row->DISMANTLE,
					'TOTAL' => (int)$row->TOTAL
				);
			}
			print json_encode($rdata);
		}else{
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}


	public function get_onair_new() {

		$result = $this->Api_model->get_onair_new();
		$rdata = array();
		if ($result){
			foreach ($result as $row) {
				$rdata[] =  array(
					'REGIONAL' => $row->REGIONAL,
					'NEW' => (int)$row->NEW,
					'TOTAL' => (int)$row->TOTAL
				);
			}
			print json_encode($rdata);
		}else{
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}

	public function get_onair_existing() {

		$result = $this->Api_model->get_onair_existing();
		$rdata = array();
		if ($result){
			foreach ($result as $row) {
				$rdata[] =  array(
					'REGIONAL' => $row->REGIONAL,
					'EXISTING' => (int)$row->EXISTING,
					'TOTAL' => (int)$row->TOTAL
				);
			}
			print json_encode($rdata);
		}else{
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}

	public function get_summary_all() {
		$result = $this->Api_model->get_summary_all();
		if ($result) {
			print json_encode($result);
		}else {
			print json_encode(array("ResponseCode"=> "000", "ResponseMessage"=> "Failed", "data" => ""));
		}
	}

	public function index()
	{
		echo "success";
	}

}
