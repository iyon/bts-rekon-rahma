<?php

/**
 * Description of MY_Model
 *
 * @author efriel @ simpol 2018
 */
class MY_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //add new record
    public function add($tbl, $data) {
        return $this->db->insert($tbl, $data);
    }

    //add new batch record
    public function add_batch($tbl, $data) {
        return $this->db->insert_batch($tbl, $data);
    }

    //update record
    public function update($tbl, $field, $id, $data) {
        $this->db->where($field, $id);
        return $this->db->update($tbl, $data);
    }

    //update batch record
    public function update_batch($tbl, $data, $id) {
        return $this->db->update_batch($tbl, $data, $id);
    }

    //delete record
    public function delete($tbl, $field, $id) {
        $this->db->where($field, $id);
        $sql = $this->db->delete($tbl);
        return $sql;
    }

    //get by id
    public function get_id($tbl, $filed, $id) {
        $this->db->where('active', 1);
        $this->db->where('id', $this->session->userdata('id'));
        $this->db->where($filed, $id);
        return $this->db->get($tbl);
    }


    //get by id simple
    public function get_id_simple($tbl, $field, $id) {
        $this->db->where($field, $id);
        return $this->db->get($tbl);
    }

    //total all
    public function total_all($tbl) {
        $this->db->select('count(*) as total');
        $this->db->from($tbl);
        $this->db->where('active', 1);
        $this->db->where('id', $this->session->userdata('id'));
        return $this->db->get();
    }

    //total all simple
    public function total_all_simple($tbl) {
        $this->db->select('count(*) as total');
        $this->db->from($tbl);
        return $this->db->get();
    }


    public function get_auto_id($tbl, $field) {
        $sql = $this->db->query("SELECT MAX( " . $field . " ) AS " . $field . " FROM " . $tbl);
        return $sql->result();
    }


}
